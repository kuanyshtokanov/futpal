import { Router } from 'express';
import GamesController from '@controllers/games.controller';
import { ClearPlayersListDto, CreateGameDto, EditGameDto, EditPlayersDto, LeaveGameDto, PlayerInfoDto } from '@dtos/games.dto';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from '@middlewares/auth.middleware';
import validationMiddleware from '@middlewares/validation.middleware';
// import { permissionsMiddleware } from '@middlewares/permissions.middleware';
// import { Roles } from '@consts/roles.consts';

class GamesRoute implements Routes {
  public path = '/games';
  public router = Router();
  public gamesController = new GamesController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.gamesController.getAllGames);
    this.router.get(`${this.path}/filter`, this.gamesController.getGames);
    this.router.get(`${this.path}/upcoming`, this.gamesController.getUpcomingGames);
    this.router.get(`${this.path}/past`, this.gamesController.getPastGames);
    this.router.get(`${this.path}/get-game/:id`, this.gamesController.getGameByAppointmentType);
    this.router.get(`${this.path}/get-game-v2/:id`, this.gamesController.getGameByAppointmentTypeNew);
    this.router.get(`${this.path}/add-players`, this.gamesController.getAddPlayersToGame);
    this.router.post(
      `${this.path}`,
      validationMiddleware(CreateGameDto, 'body'),
      authMiddleware,
      this.gamesController.postCreateGame
    );
    this.router.put(
      `${this.path}/:id`,
      validationMiddleware(EditGameDto, 'body'),
      authMiddleware,
      this.gamesController.putEditGame
    );
    this.router.post(
      `${this.path}/join/:id`,
      validationMiddleware(PlayerInfoDto, 'body'),
      authMiddleware,
      this.gamesController.postJoinGame
    );
    this.router.post(
      `${this.path}/leave/:id`,
      validationMiddleware(LeaveGameDto, 'body'),
      authMiddleware,
      this.gamesController.postLeaveGame
    );
    this.router.put(
      `${this.path}/:id/players`,
      validationMiddleware(EditPlayersDto, 'body'),
      this.gamesController.putEditPlayersList
    );
    this.router.post(
      `${this.path}/add-players`,
      // validationMiddleware(AddPlayersToDameDto, 'body'),
      this.gamesController.postAddSinglePlayerToGame
    );
    this.router.post(
      `${this.path}/clear`,
      validationMiddleware(ClearPlayersListDto, 'body'),
      authMiddleware,
      this.gamesController.clearPlayersListByAppointmentType
    );
    this.router.get(
      `${this.path}/:id`,
      authMiddleware,
      // permissionsMiddleware({ hasRole: [Roles.ADMIN, Roles.GAME_ORGANIZER, Roles.PLAYER] }),
      this.gamesController.getGameById
    );
  }
}

export default GamesRoute;
