import { HttpException } from "@exceptions/HttpException";
import { GameGroup } from "@interfaces/game.group.interface";
import gameGroupModel from "@models/game.groups.model";
import { GameGroupStatus } from "@consts/game.group.statuses";
import GameGroupsService from "@services/game.group.service";

const gameGroupsService = new GameGroupsService();

export const updateGameGroups = async (): Promise<boolean> => {
  try {
    await gameGroupsService.findAllActiveGameGroups().then(async gameGroups => {
      for (let item of gameGroups) {
        await checkGameTypeActive(item);
      }
    });
    return true;
  } catch (ex) {
    // console.log('--catch ex--', ex)
    return false;
  }
}

const checkGameTypeActive = async(gameType: GameGroup) => {
  let updated: GameGroup| null = gameType;

  try{
    if (new Date(gameType.endDate) < new Date()){
      updated = await gameGroupModel.findOneAndUpdate({ _id: gameType._id },
        {status: GameGroupStatus[GameGroupStatus.OFF]},
        {new: false}
      );
    }
  } catch(ex) {
    throw new HttpException(500, 'error while updating game type ' + gameType._id);
  } finally {

  }
}