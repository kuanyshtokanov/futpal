import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { RequestHandler } from 'express';
import { HttpException } from '@exceptions/HttpException';

const validationMiddleware = (
  type: any,
  value: 'body' | 'query' | 'params' = 'body',
  skipMissingProperties = false,
  whitelist = true,
): RequestHandler => {
  return (req, res, next) => {
    try{
      validate(plainToClass(type, req[value]),
      {
        skipMissingProperties,
        whitelist,
      }).then((errors: ValidationError[]) => {
        if (errors.length > 0) {
          const message = errors.filter((error: ValidationError) => error.constraints && Object.values(error.constraints));
          if (message && message.length > 0){
            next(new HttpException(400, message.join(', ')));
          } else {
            next(new HttpException(400, 'Error while validating body'));
          }
        } else {
          next();
        }
      });
    } catch (error) {
      next(new HttpException(400, 'Validation error'));
    }
  };
};

export default validationMiddleware;
