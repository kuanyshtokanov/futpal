import { Transaction } from '@interfaces/transaction.interface';
import { model, Schema, Document } from 'mongoose';


const transactionSchema: Schema = new Schema(
  {
    gameId: {
      type: String,
      required: true,
    },
    stripeTransactionId: {
      type: String,
      required: true,
    },
    receiptUrl: {
      type: String,
      required: true,
    },
    createdBy: {
      type: String
    },
    stripeStatus: {
      type: String,
      required: true,
    },
    receiptEmail: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    currency: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true,
    }
  }
);

const transactionModel = model<Transaction & Document>('transactions', transactionSchema);

export default transactionModel;
