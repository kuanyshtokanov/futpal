import { model, Schema, Document, Mongoose } from 'mongoose';

import { GameStatus } from '@consts/game.statuses';
import { Game } from '@interfaces/games.interface';
import { GamePaymentType } from '@consts/game.payment.types';

const playerInfoSchema = new Schema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
  },
  photoUrl: {
    type: String,
  }
});

const gameSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    code: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: false,
    },
    appointmentType: {
      type: Number,
      required: true,
    },
    createdBy: {
      type: String
    },
    status: {
      type: String,
      enum : GameStatus,
      default: GameStatus.ACTIVE
    },
    gameGroup: {
      type: Schema.Types.ObjectId
    },
    time: {
      type: Date
    },
    playersList: {
      type: [playerInfoSchema],
      default: []
    },
    redirectUrl: {
      type: String,
    },
    price: {
      type: Number,
      required: true,
    },
    duration: {
      type: Number,
      required: true,
    },
    numOfPlayers: {
      type: Number,
      required: true,
    },
    paymentType: {
      type: String,
      enum : GamePaymentType,
      default: GamePaymentType.FREE
    },
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true,
    }
  }
);

const gameModel = model<Game & Document>('games', gameSchema);

export default gameModel;
