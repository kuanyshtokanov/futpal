export interface Game {
  _id: string;
  name: string;
  description: string;
  createdBy: string;
  createdAt: Date;
  status: string;
  time: string;
  playersList: PlayerInfo[];
  redirectUrl: string;
  appointmentType: number;
  price: number;
  duration: number;
  numOfPlayers: number;
  paymentType?: string;
}

export interface PlayerInfo {
  firstName: string;
  lastName: string;
  email: string;
  photoUrl?: string;
}
