import { CreateTransactionDto } from "@dtos/payment.dto";
import { HttpException } from "@exceptions/HttpException";
import { Transaction } from "@interfaces/transaction.interface";
import transactionModel from "@models/transactions.model";
import { isEmpty } from '@utils/util';

class TransactionsService {
  public transactions = transactionModel;

  public async findAllTransactions(): Promise<Transaction[]> {
    const transactions: Transaction[] = await this.transactions.find();
    return transactions;
  }

  public async findTransactionById(transactionId: string): Promise<Transaction> {
    if (isEmpty(transactionId)) throw new HttpException(400, "gameId not provided");

    const findTransaction: Transaction | null = await this.transactions.findOne({ _id: transactionId });
    if (!findTransaction) throw new HttpException(409, "Game not found");

    return findTransaction;
  }

  public async createTransaction(transactionData: CreateTransactionDto): Promise<Transaction> {
    if (isEmpty(transactionData)) throw new HttpException(400, "gameData not provided");

    const createTransactionData: Transaction = await this.transactions.create({ ...transactionData });

    return createTransactionData;
  };
}

export default TransactionsService;
