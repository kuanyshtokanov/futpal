export interface User {
  _id: string;
  uid: string;
  password: string;
  displayName: string;
  email: string;
  emailVerified: boolean;
  disabled: boolean;
  role: string;
  photoUrl: string;
  authProvider: string;
  token?: string;
}

export interface UserSession {
  name: string;
  email: string;
  role: string;
}