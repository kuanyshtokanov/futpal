import { GameGroupStatus } from '@consts/game.group.statuses';
import { CreateGameGroupDto, EditGameGroupDto } from '@dtos/game.groups.dto';
import { HttpException } from '@exceptions/HttpException';
import { GameGroup } from '@interfaces/game.group.interface';
import gameGroupsModel from '@models/game.groups.model';
import { isEmpty } from '@utils/util';

class GameGroupsService {
  public gameGroups = gameGroupsModel;

  public async findAllGameGroups(): Promise<GameGroup[]> {
    const gameGroups: GameGroup[] = await this.gameGroups.find();
    return gameGroups;
  }

  public async findAllActiveGameGroups(): Promise<GameGroup[]> {
    const activeGameGroups: GameGroup[] = await this.gameGroups.find({status: GameGroupStatus[GameGroupStatus.ON]});
    return activeGameGroups;
  }

  public async findGameGroupById(gameGroupId: string): Promise<GameGroup> {
    if (isEmpty(gameGroupId)) throw new HttpException(400, "gameId not provided");

    const findGameGroup: GameGroup | null = await this.gameGroups.findOne({ _id: gameGroupId });
    if (!findGameGroup) throw new HttpException(409, "GameGroup not found");

    return findGameGroup;
  }

  public async findGameGroupByAptType(appointmentType: number): Promise<GameGroup> {
    if (isEmpty(appointmentType)) throw new HttpException(400, "appointmentType not provided");

    const findGameGroup: GameGroup | null = await this.gameGroups.findOne({ appointmentType: appointmentType, status: GameGroupStatus[GameGroupStatus.ON] });
    if (!findGameGroup) throw new HttpException(409, "GameGroup not found");

    return findGameGroup;
  }

  public async createGameGroup(gameGroupData: CreateGameGroupDto): Promise<GameGroup> {
    if (isEmpty(gameGroupData)) throw new HttpException(400, "gameData not provided");

    const findGame: GameGroup | null = await this.gameGroups.findOne({ appointmentType: gameGroupData.appointmentType });
    if (findGame) throw new HttpException(409, `Game type with this appointment type ${gameGroupData.appointmentType} already exists`);

    const createGameGroupData: GameGroup = await this.gameGroups.create({ ...gameGroupData });

    return createGameGroupData;
  };

  public async editGameGroup(gameGroupId: string, gameGroupData: EditGameGroupDto) {
    if (isEmpty(gameGroupData) || !gameGroupId) throw new HttpException(400, "gameData not provided");

    const updateRes = await this.gameGroups.findOneAndUpdate(
      { _id: gameGroupId },
      gameGroupData,
      {new: true}
    );
    return updateRes;
  };

};

export default GameGroupsService;