import admin from 'firebase-admin';

const credentials = require("../../firebase-key.json");

admin.initializeApp({
  credential: admin.credential.cert(credentials)
});

export default admin;