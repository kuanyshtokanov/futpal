const stripe = require('stripe')(process.env['STRIPE_KEY']);

import transactionModel from '@models/transactions.model';
import { HttpException } from '@exceptions/HttpException';
import GameService from './games.service';
import { Game } from '@interfaces/games.interface';
import TransactionsService from './transactions.service';
import { CreateTransactionDto } from '@dtos/payment.dto';
import { Transaction } from '@interfaces/transaction.interface';

class StripeService {
  public transactions = transactionModel;
  public gameService = new GameService();
  public transactionService = new TransactionsService();
  
  public async charge(gameId: string, source: any, email: string){
    const findGame: Game = await this.gameService.findGameById(gameId);
    if (!findGame) throw new HttpException(409, "Game not found");

    const {id} = source;

    const charge = await stripe.charges.create({
      amount: findGame.price*100,
      description: findGame.name,
      currency: 'eur',
      source: id,
      receipt_email: email,
    });

    if (!charge) throw new HttpException(400, 'charge unsuccessful');

    const createTransaction: CreateTransactionDto = {
      receiptEmail: email,
      gameId: gameId,
      source: source,
      amount: findGame.price,
      currency: charge.currency,
      stripeTransactionId: charge.id,
      stripeStatus: charge.status,
      receiptUrl: charge.receipt_url,
    }
    await this.transactionService.createTransaction(createTransaction);

    return charge;
  }

}
export default StripeService;