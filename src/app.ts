/**
 * Required External Modules
 */
process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

//  import * as dotenv from "dotenv";
import express from "express";
// import session from "express-session";
import cors from "cors";
import config from 'config';
import { connect, set } from 'mongoose';
import 'reflect-metadata';

import helmet from "helmet";
import hpp from 'hpp';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import * as cron from 'node-cron';

import { oneDay } from "@consts/app.consts";
import { dbConnection } from '@databases';
import { Routes } from '@interfaces/routes.interface';
import { SessionStore } from '@interfaces/auth.interface';
import errorMiddleware from '@middlewares/error.middleware';
import { logger, stream } from '@utils/logger';
import { clearListJob } from "@utils/clearListJob";
import { updateGameGroups } from "./utils/updateFinishedGameGroups";
 
class App {
/**
 * App Variables
*/
  public app: express.Application;
  public port: string | number;
  public env: string;
  private sess: SessionStore = {
    secret: 'futpal awesome',
    cookie: {
      maxAge: oneDay,
    }
  };

  constructor(routes: Routes[]) {
    this.app = express();
    this.port = process.env.PORT || 3000;
    this.env = process.env.NODE_ENV || 'development';

    this.connectToDatabase();
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    // this.initializeSwagger();
    this.initializeErrorHandling();
    this.initializeCronJob();
  }

/**
 * Server Activation
*/
  public listen() {
    this.app.listen(this.port, () => {
      logger.info(`=================================`);
      logger.info(`======= ENV: ${this.env} =======`);
      logger.info(`🚀 App listening on the port ${this.port}`);
      logger.info(`=================================`);
    });
  }

  public getServer() {
    return this.app;
  }

  private connectToDatabase() {
    if (this.env !== 'production') {
      set('debug', true);
    }

    connect(dbConnection.url);
  }

  // private initializeSession(){
  //   if (this.env === 'production') {
  //       this.app.set('trust proxy', 1) // trust first proxy
  //       this.sess.cookie.secure = true // serve secure cookies
  //     }
      
  //     this.app.use(session(this.sess))
  // }

/**
 *  App Configuration
*/
  private initializeMiddlewares() {
    this.app.use(morgan(config.get('log.format'), { stream }));
    this.app.use(cors({ origin: config.get('cors.origin'), credentials: config.get('cors.credentials') }));
    this.app.use(hpp());
    this.app.use(helmet());
    // this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
    // this.initializeSession();
  }

  private initializeRoutes(routes: Routes[]) {
    routes.forEach(route => {
      this.app.use('/', route.router);
    });
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeCronJob() {
    cron.schedule('0 0 0 * * *', () => {
      console.log('running a updateGameGroups task every day at 00:00');
      updateGameGroups();
    });

    cron.schedule('0 0 1 * * *', () => {
      console.log('running a clearListJob task every day at 01:00');
      clearListJob();
    });
  }
}

export default App;
