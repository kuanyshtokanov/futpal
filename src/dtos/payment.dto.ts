import { IsEmail, IsString, IsNumber, IsNotEmpty, IsCurrency } from 'class-validator';

export class CreatePaymentDto {
  @IsEmail()
  public receipt_email!: string;

  public source!: object;

  @IsString()
  public gameId!: string;
}

export class CreateTransactionDto {
  @IsEmail()
  public receiptEmail!: string;

  public source!: object;

  @IsString()
  public gameId!: string;

  @IsString()
  @IsNotEmpty()
  public stripeTransactionId!: string;

  @IsNumber()
  public amount!: number;

  @IsCurrency()
  public currency!: string;

  @IsNotEmpty()
  public stripeStatus!: string;

  @IsNotEmpty()
  public receiptUrl!: string;
}