process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import 'dotenv/config';
import 'module-alias/register';
import App from './app';
import validateEnv from '@utils/validateEnv';

import IndexRoute from '@routes/index.route';
import AuthRoute from '@routes/auth.route';
import UsersRoute from '@routes/users.route';
import GamesRoute from '@routes/games.route';
import GameGroupsRoute from '@routes/game.groups.route';
import PaymentsRoute from '@routes/payment.route';

validateEnv();

const app = new App(
  [
    new IndexRoute(),
    new AuthRoute(),
    new UsersRoute(),
    new GamesRoute(),
    new GameGroupsRoute(),
    new PaymentsRoute(),
  ]
);

app.listen();