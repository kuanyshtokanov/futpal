import { model, Schema, Document } from 'mongoose';

import { GameGroupStatus } from '@consts/game.group.statuses';
import { GameFrequencyType } from '@consts/frequency.types';
import { GameGroup } from '@interfaces/game.group.interface';
import { RepeatType } from '@consts/repeat.types';
import { GamePaymentType } from '@consts/game.payment.types';

const frequencySchema = new Schema({
  repeat: {
    type: String,
    enum: RepeatType,
    default: RepeatType.WEEKLY,
  },
  weekDays: {
    type: [Number],
  },
  monthDays: {
    type: [Number],
  },
});

const gameGroupSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    code: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: false,
    },
    appointmentType: {
      type: Number,
      required: true,
    },
    createdBy: {
      type: String
    },
    status: {
      type: String,
      enum : GameGroupStatus,
      default: GameGroupStatus.ON
    },
    frequencyType: {
      type: String,
      enum : GameFrequencyType,
      default: GameFrequencyType.RECURRING
    },
    frequency: {
      type: frequencySchema
    },
    time: {
      type: String
    },
    startDate: {
      type: Date
    },
    endDate: {
      type: Date
    },
    redirectUrl: {
      type: String,
    },
    price: {
      type: Number,
      required: true,
    },
    duration: {
      type: Number,
      required: true,
    },
    numOfPlayers: {
      type: Number,
      required: true,
    },
    paymentType: {
      type: String,
      enum : GamePaymentType,
      default: GamePaymentType.FREE
    },
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true,
    }
  }
);

const gameGroupModel = model<GameGroup & Document>('game_groups', gameGroupSchema);

export default gameGroupModel;
