export interface Transaction {
  _id: string;
  gameId: string;
  amount: string;
  receiptEmail: string;
  createdBy: string;
  createdAt: Date;
  status: string;
}