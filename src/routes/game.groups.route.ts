import { Router } from 'express';

import GamesGroupsController from '@controllers/game.groups.controller';
import { Routes } from '@interfaces/routes.interface';
import { CreateGameGroupDto, EditGameGroupDto } from '@dtos/game.groups.dto';
import validationMiddleware from '@middlewares/validation.middleware';
import authMiddleware from '@middlewares/auth.middleware';

class GameGroupsRoute implements Routes {
  public path = '/game-groups';
  public router = Router();
  public gameGroupsController = new GamesGroupsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.gameGroupsController.getGameGroups);
    this.router.get(`${this.path}/:id`, this.gameGroupsController.getGameGroupById);
    this.router.post(
      `${this.path}`,
      validationMiddleware(CreateGameGroupDto, 'body'),
      authMiddleware,
      this.gameGroupsController.postCreateGameGroup
    );
    this.router.put(
      `${this.path}/:id`,
      validationMiddleware(EditGameGroupDto, 'body'),
      authMiddleware,
      this.gameGroupsController.putEditGameGroup
    );
  }
}

export default GameGroupsRoute;
