import { GameStatus } from "@consts/game.statuses";
import { RepeatType } from "@consts/repeat.types";
import { CreateGameDto } from "@dtos/games.dto";
import { GameGroup } from "@interfaces/game.group.interface";
import { Game } from "@interfaces/games.interface";
import { getNextDayOfTheWeek } from "./util";
import gameModel from '@models/games.model';
import { GamePaymentType } from "@consts/game.payment.types";

export const generateGames = async (gameGroup: GameGroup): Promise<Game[]> => {
  const newGame: CreateGameDto = new CreateGameDto();
  newGame.appointmentType = gameGroup.appointmentType;
  newGame.gameGroup = gameGroup._id;
  newGame.redirectUrl = gameGroup.redirectUrl;
  newGame.name = gameGroup.name;
  newGame.address = gameGroup.address;
  newGame.code = gameGroup.code;
  newGame.duration = gameGroup.duration;
  newGame.price = gameGroup.price;
  newGame.numOfPlayers = gameGroup.numOfPlayers;
  newGame.status = GameStatus[GameStatus.ACTIVE] as unknown as GameStatus;
  newGame.description = gameGroup.description;
  newGame.paymentType = gameGroup.paymentType as unknown as GamePaymentType;

  let games: Game[] = [];

  if (gameGroup.frequency.repeat === RepeatType.WEEKLY){
    for (let freq of gameGroup.frequency.weekDays){
      const nextTime = getNextDayOfTheWeek(freq && freq > 6 ? 0: freq, gameGroup.time)
      const reccurentGames = createReccurentGames(nextTime, gameGroup.endDate, newGame);
      games = await gameModel.insertMany(reccurentGames).then(items => items as Game[]);
    }
  }
  return games;
}

const createReccurentGames = (start: string | number | Date, end: string | number | Date, game: CreateGameDto): CreateGameDto[] => {
  let startDate = new Date(start);
  const endDate = new Date(end);
  const result = [];
    while (startDate < endDate) {
      let newGame = {...game};
      newGame.time = startDate.toISOString();
      result.push(newGame);
      startDate = new Date(startDate);
      startDate.setUTCDate(startDate.getUTCDate() + 7);
    }
    return result;
}