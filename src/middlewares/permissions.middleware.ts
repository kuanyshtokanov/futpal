import { Request, Response } from "express";

import { Roles } from "@consts/roles.consts";

export function permissionsMiddleware(opts: { hasRole: Array<Roles>, allowSameUser?: boolean }) {
  return (req: Request, res: Response, next: Function) => {
    // console.log('--res.locals--', res.locals)
    const { role, email, uid: localUid } = res.locals
    const { id: paramsId } = req.params
    const { id: bodyId, uid: bodyUid } = req.body;
    const id = paramsId ?? bodyId ?? bodyUid ?? null;

    if (opts.allowSameUser && id && localUid === id)
      return next();

    if (!role)
      return res.status(403).send();

    if (opts.hasRole.includes(role))
      return next();

    return res.status(403).send();
  }
}