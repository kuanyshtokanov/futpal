import { NextFunction, Request, Response } from 'express';

import { Transaction } from "@interfaces/transaction.interface";
import TransactionsService from "@services/transactions.service";

class TransactionsController {
  public transactionsService = new TransactionsService();
  
  public getTransactions = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findAllTransactionsData: Transaction[] = await this.transactionsService.findAllTransactions();

      res.status(200).json({ data: findAllTransactionsData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getTransactionById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const transactionId: string = req.params.id;
      const findOneTransactionData: Transaction = await this.transactionsService.findTransactionById(transactionId);

      res.status(200).json({ data: findOneTransactionData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };
}

export default TransactionsController;
