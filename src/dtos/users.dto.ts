import { IsEmail, IsString, IsEnum, IsBoolean, IsOptional } from 'class-validator';

import { Roles } from '@consts/roles.consts';

export class CreateUserDto {
  @IsEmail()
  public email!: string;

  @IsBoolean()
  public emailVerified!: boolean;

  @IsString()
  public uid!: string;

  @IsOptional()
  public password!: string;

  @IsString()
  public displayName!: string;

  @IsOptional()
  @IsString()
  public photoUrl!: string;

  @IsBoolean()
  public disabled: boolean = false;

  @IsString()
  public authProvider!: string;

  @IsEnum(Roles)
  public role!: Roles;
}

export class LoginUserDto {
  @IsEmail()
  public email!: string;

  @IsString()
  public password!: string;
}
