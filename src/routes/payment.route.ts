import { Router } from 'express';

import { Routes } from '@interfaces/routes.interface';
import PaymentController from '@controllers/payment.controller';
import authMiddleware from '@middlewares/auth.middleware';
import validationMiddleware from '@middlewares/validation.middleware';
import { CreatePaymentDto } from '@dtos/payment.dto';
import TransactionsController from '@controllers/transaction.controller';

class PaymentsRoute implements Routes {
  public path = '/payments';
  public router = Router();
  public paymentController = new PaymentController();
  public transactionsController = new TransactionsController();


  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}/transactions`,
      authMiddleware,
      this.transactionsController.getTransactions
    );
    this.router.get(
      `${this.path}/transactions/:id`,
      authMiddleware,
      this.transactionsController.getTransactionById
    );
    this.router.post(
      `${this.path}/stripe`,
      authMiddleware,
      validationMiddleware(CreatePaymentDto, 'body'),
      this.paymentController.postCharge
    );
  }

}

export default PaymentsRoute;