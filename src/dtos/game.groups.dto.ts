import {
  IsEnum,
  IsString,
  IsNumber,
  IsOptional,
  IsDateString,
  ValidateNested,
  IsNotEmpty,
  IsArray
} from 'class-validator';

import { GameGroupStatus } from '@consts/game.group.statuses';
import { GameFrequencyType } from '@consts/frequency.types';
import { Frequency } from '@interfaces/frequency.interface';
import { Type } from 'class-transformer';
import { RepeatType } from '@consts/repeat.types';

export class CreateGameGroupDto {
  @IsString()
  public name!: string;

  @IsString()
  public code!: string;

  @IsNumber()
  public appointmentType!: number;

  @IsString()
  public description!: string;

  @IsString()
  public address!: string;

  @IsEnum(GameGroupStatus)
  public status!: GameGroupStatus;

  @IsEnum(GameFrequencyType)
  public frequencyType!: GameFrequencyType;

  @ValidateNested({each:true})
  @Type(() => FrequencyDto)
  frequency!: Frequency;

  public time?: string;

  @IsDateString()
  public startDate!: string;

  @IsDateString()
  public endDate!: string;

  @IsOptional()
  public createdBy?: string;

  @IsString()
  public redirectUrl!: string;

  @IsNumber()
  price!: number;

  @IsNumber()
  duration!: number;

  @IsNumber()
  numOfPlayers!: number;
};

export class EditGameGroupDto {
  @IsString()
  public name!: string;

  @IsString()
  public code!: string;

  @IsOptional()
  @IsString()
  public description!: string;

  @IsString()
  public address!: string;

  @IsEnum(GameGroupStatus)
  public status!: GameGroupStatus;

  @IsEnum(GameFrequencyType)
  public frequencyType!: GameFrequencyType;

  @ValidateNested({each:true})
  @Type(() => FrequencyDto)
  frequency!: Frequency;

  @IsOptional()
  public time?: string;

  @IsDateString()
  public startDate!: string;

  @IsDateString()
  public endDate!: string;

  @IsOptional()
  public createdBy?: string;

  @IsString()
  public redirectUrl!: string;

  @IsNumber()
  price!: number;

  @IsNumber()
  duration!: number;

  @IsNumber()
  numOfPlayers!: number;
};

export class FrequencyDto {
  @IsNotEmpty()
  @IsEnum(RepeatType)
  repeat!: RepeatType;
  
  @IsArray()
  weekDays!: number[];

  @IsArray()
  monthDays!: number[];
};