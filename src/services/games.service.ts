import { Types } from 'mongoose';

import { GameStatus } from '@consts/game.statuses';
import { CreateGameDto, AddPlayersToDameDto, GetAddPlayersToDameDto, EditGameDto, PlayerInfoDto } from '@dtos/games.dto';
import { HttpException } from '@exceptions/HttpException';
import { Game, PlayerInfo } from '@interfaces/games.interface';
import gameModel from '@models/games.model';
import { isEmpty } from '@utils/util';
import GameGroupsService from '@services/game.group.service';
import { GameGroup } from '@interfaces/game.group.interface';

class GameService {
  public games = gameModel;
  public gameGroupsService = new GameGroupsService();

  public async findAllGames(): Promise<Game[]> {
    const games: Game[] = await this.games.find();
    return games;
  }

  public async findGames(): Promise<Game[]> {
    const games: Game[] = await this.games.find();
    return games;
  }

  public async findUpcomingGames(): Promise<Game[]> {
    const games: Game[] = await this.games.find({time: {$gt: new Date()}}).sort({time: 1});
    return games;
  }

  public async findPastGames(): Promise<Game[]> {
    const games: Game[] = await this.games.find({time: {$lt: new Date()}}).sort({time: -1});
    return games;
  }

  public async findGameById(gameId: string): Promise<Game> {
    if (isEmpty(gameId)) throw new HttpException(400, "gameId not provided");

    const findGame: Game | null = await this.games.findOne({ _id: gameId });
    if (!findGame) throw new HttpException(409, "Game not found");

    return findGame;
  }

  public async findGameByGameGroupId(gameGroupId: string): Promise<Game | null> {
    if (isEmpty(gameGroupId)) throw new HttpException(400, "gameGroupId not provided");

    const findGame: Game | null = await this.games.findOne({ gameGroup: new Types.ObjectId(gameGroupId) });

    return findGame;
  }

  public async findGamesByGameGroupId(gameGroupId: string): Promise<Game[]> {
    if (isEmpty(gameGroupId)) throw new HttpException(400, "gameGroupId not provided");

    const findGames: Game[] = await this.games.find({ gameGroup: new Types.ObjectId(gameGroupId), status: GameStatus[GameStatus.ACTIVE] });

    return findGames;
  }

  public async getGameByAppointmentType(appointmentType: number): Promise<Game> {
    if (isEmpty(appointmentType)) throw new HttpException(400, "gameId not provided");

    const findGame: Game | null = await this.games.findOne({ appointmentType: appointmentType, status: GameStatus[GameStatus.ACTIVE] });
    if (!findGame) throw new HttpException(409, "Game not found");

    return findGame;
  }

  public async getGameByAppointmentTypeNew(appointmentType: number): Promise<Game> {
    if (isEmpty(appointmentType)) throw new HttpException(400, "gameId not provided");
    const gameGroup: GameGroup = await this.gameGroupsService.findGameGroupByAptType(appointmentType);

    const findGame: Game | null = await this.games.findOne({ gameGroup: gameGroup._id });
    if (!findGame) throw new HttpException(409, "Game not found");

    return findGame;
  }

  public async createGame(gameData: CreateGameDto): Promise<Game> {
    if (isEmpty(gameData)) throw new HttpException(400, "gameData not provided");

    const findGame: Game | null = await this.games.findOne({ name: gameData.name, time: gameData.time });
    if (findGame) throw new HttpException(409, `Game with this name ${gameData.name} and for this time ${gameData.time} already exists`);

    const createGameData: Game = await this.games.create({ ...gameData });

    return createGameData;
  };

  public async editGame(gameId: string, gameData: EditGameDto) {
    if (isEmpty(gameData) || !gameId) throw new HttpException(400, "gameData not provided");

    const updateRes = await this.games.findOneAndUpdate(
      { _id: gameId },
      gameData,
      {new: true}
    );
    return updateRes;
  };

  public async joinGame(gameId: string, playerData: PlayerInfoDto) {
    if (isEmpty(playerData) || !gameId) throw new HttpException(400, "player info not provided not provided");

    const findGame: Game | null = await this.games.findOne({ _id: gameId });
    if (!findGame) throw new HttpException(409, "Game not found");
    if (findGame?.status !== 'ACTIVE'){
      throw new HttpException(400, "game is not active - cannot join");
    }

    const checkJoinedPlayers = findGame.playersList.find(list => list.email === playerData.email);

    if (checkJoinedPlayers) {
      throw new HttpException(400, "player with this email already joined - cannot join");
    }

    const updateRes = await this.games.findOneAndUpdate(
      { _id: gameId },
      { $addToSet: { playersList: playerData }},
      {new: true},
    );
    return updateRes;
  };

  public async leaveGame(gameId: string, playerEmail: string) {
    const findGame: Game | null = await this.games.findOne({ _id: gameId });
    if (!findGame) throw new HttpException(409, "Game not found");
    if (findGame?.status !== 'ACTIVE'){
      throw new HttpException(400, "game is not active - cannot leave");
    }

    const checkJoinedPlayers = findGame.playersList.find(list => list.email === playerEmail);

    if (!checkJoinedPlayers) {
      throw new HttpException(400, "player with this email not found in game");
    }

    const updateRes = await this.games.findOneAndUpdate(
      { _id: gameId },
      { $pull: { playersList: { email: playerEmail } }},
      {new: true},
    );
    return updateRes;

  }

  public async editPlayersListByGameId(gameId: string, playersData: PlayerInfoDto[]) {
    if (isEmpty(playersData) || !gameId) throw new HttpException(400, "playersData not provided");

    const updateRes = await this.games.findOneAndUpdate(
      { _id: gameId },
      playersData,
      {new: true}
    );
    return updateRes;
  };

  public async findGameByAppointmentTypeOrThrow(apptType:number): Promise<Game> {
    const findGame: Game | null = await this.games.findOne({ appointmentType: apptType });
    if (!findGame) throw new HttpException(409, `Game with this appointment type ${apptType} does not exist`);
    return findGame;
  }

  public async clearPlayersListByAppointmentType(apptType:number): Promise<Game> {
    const findGame: Game = await this.findGameByAppointmentTypeOrThrow(apptType);
    const updatedGame: any = await this.games.findByIdAndUpdate(
      { _id: findGame._id },
      {
        $set:{
          playersList: []
        }
      },
      { new: true }
    );

    return updatedGame;
  }

  public async addSinglePlayerToGame(playersData: AddPlayersToDameDto): Promise<void> {
    if (isEmpty(playersData)) throw new HttpException(400, "gameData not provided");

    const { appointmentType, playerInfo } = playersData;

    const findGame: Game = await this.findGameByAppointmentTypeOrThrow(appointmentType);
    
    let playersList: PlayerInfo[] = findGame.playersList;

    playersList.push(playerInfo);

    await this.games.updateOne(
      { _id: findGame._id },
      {
        $set:{
          playersList: playersList
        }
      }
    );
  };

  public async addPlayersToGameFromGet(playersData: GetAddPlayersToDameDto): Promise<any> {
    const { appointmentType, firstName, lastName, email } = playersData;

    const findGame: Game = await this.findGameByAppointmentTypeOrThrow(appointmentType);

    let playersList: PlayerInfo[] = findGame.playersList;
    
    const found: PlayerInfo | undefined = playersList.find(pl =>
      pl.email.toLowerCase() === email.toLowerCase()
    );
    if (found) {
      throw new HttpException(409, `${email} is already registered to appointment type ${appointmentType}`);
    } else {
      playersList.push({
        firstName,
        lastName,
        email
      });
      
      const updatedGame: any = await this.games.findByIdAndUpdate(
        { _id: findGame._id },
        {
          $set:{
            playersList: playersList
          }
        },
        { new: true }
      );

      return updatedGame;
    }
  }

}

export default GameService;
