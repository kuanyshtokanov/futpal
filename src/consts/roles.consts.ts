export enum Roles {
  ADMIN = 'ADMIN',
  GAME_ORGANIZER = 'GAME_ORGANIZER',
  PLAYER = 'PLAYER',
}