import { GameGroupStatus } from "@consts/game.group.statuses";
import { GameFrequencyType } from "@consts/frequency.types";
import { Frequency } from "./frequency.interface";
import { GamePaymentType } from "@consts/game.payment.types";

export interface GameGroup {
  _id: string;
  name: string;
  description: string;
  code: string;
  address: string;
  createdBy: string;
  createdAt: Date;
  status: keyof GameGroupStatus;
  appointmentType: number;
  frequencyType: GameFrequencyType;
  frequency: Frequency;
  time: string;
  startDate: string;
  endDate: string;
  redirectUrl: string;
  price: number;
  duration: number;
  numOfPlayers: number;
  paymentType: keyof GamePaymentType;
}
