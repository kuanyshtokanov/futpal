import { Game } from '@interfaces/games.interface';
import GameGroupsService from '@services/game.group.service';
import GameService from '@services/games.service';
import gameModel from '@models/games.model';
import { HttpException } from '@exceptions/HttpException';
import { GameStatus } from '@consts/game.statuses';

const gameGroupsService = new GameGroupsService();
const gameService = new GameService();

export const clearListJob = async (): Promise<boolean> => {
  try {
    await gameGroupsService.findAllActiveGameGroups().then(async gameGroups => {
      for (let item of gameGroups) {
        const games = await gameService.findGamesByGameGroupId(item._id.toString())
        await checkGamesStatusAndUpdate(games);
      }
    });
    return true;
  } catch (ex) {
    // console.log('--catch ex--', ex)
    return false;
  }
};

const checkGamesStatusAndUpdate = async(games: Game[]) => {
  try{
    for (let item of games) {
      if (new Date(item.time) < new Date()){
        await gameModel.findOneAndUpdate({ _id: item._id },
          {status: GameStatus[GameStatus.FINISHED]},
          {new: false}
        );
      }
    }
  } catch(ex) {
    throw new HttpException(500, 'error while updating games ');
  } finally {

  }
}