import { NextFunction, Request, Response } from 'express';

import gameGroupsService from '@services/game.group.service';
import { GameGroup } from '@interfaces/game.group.interface';
import { CreateGameGroupDto, EditGameGroupDto } from '@dtos/game.groups.dto';
import { generateGames } from '@utils/gamesUtils';

class GameGroupsController {
  public gameGroupsService = new gameGroupsService();

  public getGameGroups = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findAllGamesData: GameGroup[] = await this.gameGroupsService.findAllGameGroups();

      res.status(200).json({ data: findAllGamesData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getGameGroupById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameGroupId: string = req.params.id;
      const findOneGameTypeData: GameGroup = await this.gameGroupsService.findGameGroupById(gameGroupId);

      res.status(200).json({ data: findOneGameTypeData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public postCreateGameGroup = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameData: CreateGameGroupDto = req.body;

      const createGameGroupData: GameGroup = await this.gameGroupsService.createGameGroup(gameData);

      const createdGames = await generateGames(createGameGroupData);

      res.status(201).json({ data: createGameGroupData, message: 'game type created' + (createdGames&&createdGames.length>0?' and games are generated': ' error while generating games') });
    } catch (error) {
      next(error);
    }
  };

  public putEditGameGroup = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameGroupId: string = req.params.id;
      const gameData: EditGameGroupDto = req.body;

      const updateRes = await this.gameGroupsService.editGameGroup(gameGroupId, gameData);

      res.status(201).json({ data: updateRes, message: 'game modified' });
    } catch (error) {
      next(error);
    }
  };
}

export default GameGroupsController;