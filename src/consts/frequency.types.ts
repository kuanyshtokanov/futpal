export enum GameFrequencyType {
  ONE_TIME = 'ONE_TIME',
  RECURRING = 'RECURRING',
}