import { model, Schema, Document } from 'mongoose';

import { Roles } from '@consts/roles.consts';
import { User } from '@interfaces/users.interface';

const userSchema: Schema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    emailVerified: {
      type: Boolean,
    },
    disabled: {
      type: Boolean,
    },
    role: {
      type: String,
      enum : Roles,
      default: Roles.PLAYER
    },
    uid: {
      type: String,
      required: true,
      unique: true,
    },
    displayName: {
      type: String,
    },
    photoUrl: {
      type: String,
    },
    authProvider: {
      type: String,
    }
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true,
    }
  }
);

const userModel = model<User & Document>('users', userSchema);

export default userModel;
