import * as admin from 'firebase-admin'

import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import userModel from '@models/users.model';
import { isEmpty } from '@utils/util';

class UserService {
  public users = userModel;

  public async findAllUser(): Promise<User[]> {
    const users: User[] = await this.users.find();
    return users;
  }

  public async findUserById(userId: string): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, "UserId is empty");

    const findUser: User | null = await this.users.findOne({ _id: userId });
    if (!findUser) throw new HttpException(409, "User not found");

    return findUser;
  }

  public async findUserByUid(userUid: string): Promise<User | null> {
    if (isEmpty(userUid)) throw new HttpException(400, "UserId is empty");
    const findUser: User | null = await this.users.findOne({ uid: userUid });
    return findUser;
  }

  public async createUser(userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "No userData provided");
    
    const findUser: User | null = await this.users.findOne({ email: userData.email });
    if (findUser) throw new HttpException(409, `You're email ${userData.email} already exists`);

    const createUserData: User = await this.users.create({ ...userData });
    
    await admin.auth().setCustomUserClaims(userData.uid, { role: userData.role })

    return createUserData;
  }

  public async updateUser(userId: string, userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "No userData provided");

    if (userData.email) {
      const findUser: User | null = await this.users.findOne({ email: userData.email });
      if (findUser && findUser._id != userId) throw new HttpException(409, `You're email ${userData.email} already exists`);
    }

    const updateUserById: User | null = await this.users.findByIdAndUpdate(userId, { userData });
    if (!updateUserById) throw new HttpException(409, "User not found");

    await admin.auth().setCustomUserClaims(userData.uid, { role: userData.role });

    return updateUserById;
  }

  public async deleteUser(userId: string): Promise<User> {
    const deleteUserById: User | null = await this.users.findByIdAndDelete(userId);
    if (!deleteUserById) throw new HttpException(409, "User not found");

    return deleteUserById;
  }
}

export default UserService;
