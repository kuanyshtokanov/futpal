import { NextFunction, Request, Response } from 'express';
import {
  AddPlayersToDameDto,
  ClearPlayersListDto,
  CreateGameDto,
  EditGameDto,
  GetAddPlayersToDameDto,
  LeaveGameDto,
  PlayerInfoDto
} from '@dtos/games.dto';
import { Game } from '@interfaces/games.interface';
import gameService from '@services/games.service';

class GamesController {
  public gameService = new gameService();

  public getAllGames = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findAllGamesData: Game[] = await this.gameService.findAllGames();

      res.status(200).json({ data: findAllGamesData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getGames = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findGamesData: Game[] = await this.gameService.findAllGames();

      res.status(200).json({ data: findGamesData, message: 'findGamesByFilter' });
    } catch (error) {
      next(error);
    }
  };

  public getUpcomingGames = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findGamesData: Game[] = await this.gameService.findUpcomingGames();

      res.status(200).json({ data: findGamesData, message: 'findUpcomingGames' });
    } catch (error) {
      next(error);
    }
  };

  public getPastGames = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findGamesData: Game[] = await this.gameService.findPastGames();

      res.status(200).json({ data: findGamesData, message: 'findPastGames' });
    } catch (error) {
      next(error);
    }
  };

  public getGameById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameId: string = req.params.id;
      const findOneGameData: Game = await this.gameService.findGameById(gameId);

      res.status(200).json({ data: findOneGameData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public getGameByAppointmentType = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const appointmentId: number = Number(req.params.id);
      const findOneGameData: Game = await this.gameService.getGameByAppointmentType(appointmentId);

      res.status(200).json({ data: findOneGameData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public getGameByAppointmentTypeNew = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const appointmentId: number = Number(req.params.id);
      const findOneGameData: Game = await this.gameService.getGameByAppointmentTypeNew(appointmentId);

      res.status(200).json({ data: findOneGameData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public postCreateGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameData: CreateGameDto = req.body;

      const createGameData: Game = await this.gameService.createGame(gameData);

      res.status(201).json({ data: createGameData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public putEditGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameId: string = req.params.id;
      const gameData: EditGameDto = req.body;

      const updateRes = await this.gameService.editGame(gameId, gameData);

      res.status(201).json({ data: updateRes, message: 'game modified' });
    } catch (error) {
      next(error);
    }
  };

  public postJoinGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameId: string = req.params.id;
      const playerData: PlayerInfoDto = req.body;

      const updateRes = await this.gameService.joinGame(gameId, playerData);

      res.status(201).json({ data: updateRes, message: 'Congrats you joined game!' });
    } catch (error) {
      next(error);
    }
  };

  public postLeaveGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
    const gameId: string = req.params.id;
    const playerData: LeaveGameDto = req.body;

    const updateRes = await this.gameService.leaveGame(gameId, playerData.email);

    res.status(201).json({ data: updateRes, message: 'Congrats you left game!' });

    } catch (error) {
      next(error);
    }
  }

  public putEditPlayersList = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const gameId: string = req.params.id;
      const playersData: PlayerInfoDto[] = req.body;

      const updateRes = await this.gameService.editPlayersListByGameId(gameId, playersData);

      res.status(201).json({ data: updateRes, message: 'game modified' });
    } catch (error) {
      next(error);
    }
  };

  public clearPlayersListByAppointmentType = async (req: Request, res: Response, next: NextFunction) => {
    const reqData: ClearPlayersListDto = req.body;
    const createGameData: Game = await this.gameService.clearPlayersListByAppointmentType(reqData.appointmentType);

    res.status(201).json({ data: createGameData, message: 'list updated' });
  }

  public postAddSinglePlayerToGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const playersData: AddPlayersToDameDto = req.body;

      await this.gameService.addSinglePlayerToGame(playersData);

      res.status(201).json({ message: 'players added' });

    } catch (error) {
      next(error);
    }
  }

  public getAddPlayersToGame = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {firstName, lastName, email, appointmentType} = req.query;

      const playersData: GetAddPlayersToDameDto = {
        firstName: firstName?.toString() || '',
        lastName: lastName?.toString() || '',
        email: email?.toString() || '',
        appointmentType: Number(appointmentType) || 0,
      }

      const modifiedGame: Game = await this.gameService.addPlayersToGameFromGet(playersData);

      res.writeHead(301, {
        Location: `https://futpal.net/${modifiedGame.redirectUrl}`
      }).end();

    } catch (error) {
      next(error);
    }
  }

}

export default GamesController;
