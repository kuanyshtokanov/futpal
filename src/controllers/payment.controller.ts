import { NextFunction, Request, Response } from 'express';

import StripeService from '@services/stripe.service';

class PaymentController {
  public paymentService = new StripeService();

  public postCharge = async (req: Request, res: Response, next: NextFunction) => {
    try{
      const { gameId, source, receipt_email } = req.body;

      const paymentResponse: any = await this.paymentService.charge(gameId, source, receipt_email);

      res.status(200).json({ data: paymentResponse, message: 'payment succeeded' });

    } catch (error) {
      next(error);
    }
  }
}

export default PaymentController;