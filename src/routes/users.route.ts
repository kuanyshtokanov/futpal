import { Router } from 'express';
import UsersController from '@controllers/users.controller';
import { all, create, get, patch, remove } from '@controllers/firebase.users.controller';
import { CreateUserDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from '@middlewares/auth.middleware';
import validationMiddleware from '@middlewares/validation.middleware';
import { permissionsMiddleware } from '@middlewares/permissions.middleware';
import { Roles } from '@consts/roles.consts';

class UsersRoute implements Routes {
  public path = '/users';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    // this.router.get(`${this.path}`, authMiddleware, this.usersController.getUsers);
    // this.router.get(`${this.path}/:id`, this.usersController.getUserById);
    // this.router.post(
    //   `${this.path}`,
    //   validationMiddleware(CreateUserDto, 'body'),
    //   this.usersController.createUser
    // );
    this.router.post(
      `${this.path}/create`,
      authMiddleware,
      permissionsMiddleware({ hasRole: [Roles.ADMIN], allowSameUser: true }),
      validationMiddleware(CreateUserDto, 'body'),
      this.usersController.createUser
    );
    // this.router.put(
    //   `${this.path}/:id`,
    //   validationMiddleware(CreateUserDto, 'body', true),
    //   this.usersController.updateUser
    // );
    // this.router.delete(`${this.path}/:id`, this.usersController.deleteUser);

    this.router.get('/users', [
      authMiddleware,
      permissionsMiddleware({ hasRole: [Roles.ADMIN] }),
      this.usersController.getUsers
    ]);
  // get :id user
    this.router.get('/users/:id', [
      authMiddleware,
      permissionsMiddleware({ hasRole: [Roles.ADMIN], allowSameUser: true  }),
      this.usersController.getUserById
    ]);
    this.router.get('/users/uid/:id', [
      authMiddleware,
      permissionsMiddleware({ hasRole: [Roles.ADMIN], allowSameUser: true  }),
      this.usersController.getUserByUid
    ]);
  // updates :id user
    this.router.patch('/users/:id', [
      authMiddleware,
      permissionsMiddleware({ hasRole: [Roles.ADMIN], allowSameUser: true  }),
      validationMiddleware(CreateUserDto, 'body'),
      this.usersController.updateUser
    ]);
  // deletes :id user
    // this.router.delete('/users/:id', [
    //   authMiddleware,
    //   permissionsMiddleware({ hasRole: [Roles.ADMIN] }),
    //   remove
    // ]);
  }
}

export default UsersRoute;
