import { IsEnum, IsString, IsNumber, IsOptional, ValidateNested, IsArray, IsNotEmpty } from 'class-validator';

import { GameStatus } from '@consts/game.statuses';
import { Type } from 'class-transformer';
import { GamePaymentType } from '@consts/game.payment.types';

export class CreateGameDto {
  @IsString()
  public name!: string;

  @IsString()
  public code!: string;

  @IsNumber()
  public appointmentType!: number;

  @IsString()
  public description!: string;

  @IsString()
  public address!: string;

  @IsEnum(GameStatus)
  public status!: GameStatus;

  @IsOptional()
  public time?: string;

  @IsOptional()
  public createdBy?: string;

  @IsString()
  public redirectUrl!: string;

  @IsNumber()
  price!: number;

  @IsNumber()
  duration!: number;

  @IsNumber()
  numOfPlayers!: number;
  
  @IsString()
  public gameGroup!: string;

  @IsEnum(GamePaymentType)
  public paymentType!: GamePaymentType;
};

export class EditGameDto {
  @IsString()
  public name!: string;

  @IsString()
  public code!: string;

  @IsOptional()
  @IsString()
  public description!: string;

  @IsString()
  public address!: string;

  @IsEnum(GameStatus)
  public status!: GameStatus;

  @IsOptional()
  public time?: string;

  @IsOptional()
  public createdBy?: string;

  @IsString()
  public redirectUrl!: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({each:true})
  @Type(() => PlayerInfoDto)
  public playersList!: PlayerInfoDto[];

  @IsNumber()
  price!: number;

  @IsNumber()
  duration!: number;

  @IsNumber()
  numOfPlayers!: number;

  @IsEnum(GamePaymentType)
  public paymentType!: GamePaymentType;
};

export class AddPlayersToDameDto {
  playerInfo!: {
    firstName: string;
    lastName: string;
    email: string;
  };

  @IsNumber()
  appointmentType!: number;
}

export class GetAddPlayersToDameDto {
  firstName!: string;
  lastName!: string;
  email!: string;
  appointmentType!: number;
}

export class ClearPlayersListDto {
  @IsNumber()
  appointmentType!: number;
}

export class PlayerInfoDto {
  @IsNotEmpty()
  @IsString()
  firstName!: string;
  
  @IsNotEmpty()
  @IsString()
  lastName!: string;

  @IsOptional()
  @IsString()
  photoUrl!: string;

  @IsNotEmpty()
  @IsString()
  email!: string;
};

export class EditPlayersDto {
  @IsOptional()
  @IsArray()
  @ValidateNested({each:true})
  @Type(() => PlayerInfoDto)
  public playersList!: PlayerInfoDto[];
}

export class LeaveGameDto {
  @IsNotEmpty()
  @IsString()
  email!: string;
};