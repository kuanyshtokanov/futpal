import { RepeatType } from "@consts/repeat.types";

export interface Frequency {
  repeat: RepeatType;
  weekDays: number[];
  monthDays: number[];
}